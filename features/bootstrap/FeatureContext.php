<?php

use Behat\MinkExtension\Context\RawMinkContext;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;

/**
 * Base context
 */
class FeatureContext extends RawMinkContext
{
    function visitPath($path)
    {
        $this->getSession()->visit($this->locatePath($path));
    }
}
