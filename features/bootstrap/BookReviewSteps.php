<?php

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\SnippetAcceptingContext;

class BookReviewSteps extends FeatureContext implements SnippetAcceptingContext
{
    /**
     * @Given I am on the book reviews page
     */
    function iAmOnTheBookReviewsPage()
    {
        $this->visitPath("/books");
    }

    /**
     * @Given I am on the new book review page
     */
    function iAmOnTheNewBookReviewPage()
    {
        $this->visitPath("/books/new");
    }
}
