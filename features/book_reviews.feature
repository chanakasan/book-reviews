Feature: Book Reviews
    As a logged in user
    I should be able mange book reviews

    Background:
        Given I am on "/login"
        And I fill in "username" with "chanaka"
        And I fill in "password" with "pass123"
        And I press "Login" 

    Scenario: Visit book reviews page
        Given I am on the book reviews page
        Then I should see "Book Reviews"
        Then I should see "Add Review"
    
    Scenario: Successfully add book review
        Given I am on the new book review page
        Then I should see "New Review"
        When I fill in "book_title" with "Harry Potter and the Chamber of Secrets"
        When I fill in "book_author" with "J. K. Rowling"
        When I fill in "book_summary" with "hmm..."
        When I fill in "book_review" with "It's well written."
        And I press "submit"
        Then I should see "Harry Potter and the Chamber of Secrets"
