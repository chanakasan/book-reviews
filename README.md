### Installation ###
Follow these steps to install and run this app.

* Download the code and change directory to the root folder in a Terminal app.
```
cd book-reviews-app/
```

* Install composer and install libraries
```
php -r "readfile('https://getcomposer.org/installer');" | php
php composer.phar install
```

* Set your database name and credentials in parameters.yml

* Create the database and schema
```
app/console doctrine:database:create
app/console doctrine:schema:update --force
```

* Run the app (start the server)
```
app/console server:run
```

* Visit `http://localhost:8000/` to see the homepage.
