<?php

namespace Review\MainBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Review", mappedBy="reviewedBy")
     */
    private $reviews;

    function __construct()
    {
        parent::__construct();
        $this->reviews = new ArrayCollection();
    }

    /**
     * @return array
     */
    function getReviews()
    {
        return $this->reviews->toArray();
    }

    function addReview(Review $review)
    {
        $this->reviews->add($review);
    }

    function removeReview(Review $review)
    {
        $this->reviews->removeElement($review);
    }

    /**
     * @param mixed $reviews
     */
    function setReviews(array $reviews)
    {
        foreach ($reviews as $review) {
            $this->addReview($review);
        }
    }
}
