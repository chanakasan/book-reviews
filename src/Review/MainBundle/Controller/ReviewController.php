<?php

namespace Review\MainBundle\Controller;

use Doctrine\Common\Util\Debug;
use Review\MainBundle\Entity\Comment;
use Review\MainBundle\Form\CommentType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Review\MainBundle\Entity\Review;
use Review\MainBundle\Form\ReviewType;

/**
 * Review controller.
 *
 * @Route("/books")
 */
class ReviewController extends Controller
{
    /**
     * Lists all Review entities.
     *
     * @Route("/", name="books")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('ReviewMainBundle:Review')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Review entity.
     *
     * @Route("/", name="books_create")
     * @Method("POST")
     * @Template("ReviewMainBundle:Review:new.html.twig")
     */
    public function createAction(Request $request)
    {
        if ( ! $this->checkAuth()) return $this->accessDeniedResponse();

        $entity = new Review();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $entity->setReviewedBy($this->getCurrentUser());

            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('books_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Review entity.
     *
     * @param Review $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Review $entity)
    {
        $form = $this->createForm(new ReviewType(), $entity, array(
            'action' => $this->generateUrl('books_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Review entity.
     *
     * @Route("/new", name="books_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        if ( ! $this->checkAuth()) return $this->accessDeniedResponse();

        $entity = new Review();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Review entity.
     *
     * @Route("/{id}", name="books_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReviewMainBundle:Review')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Review entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        
        $comments = $em->getRepository('ReviewMainBundle:Comment')->findBy(['review' => $entity]);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
            'comments' => $comments,
            'comment_form' => $this->createCommentForm($id)->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Review entity.
     *
     * @Route("/{id}/edit", name="books_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        if ( ! $this->checkAuth()) return $this->accessDeniedResponse();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReviewMainBundle:Review')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Review entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Review entity.
    *
    * @param Review $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Review $entity)
    {
        $form = $this->createForm(new ReviewType(), $entity, array(
            'action' => $this->generateUrl('books_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Review entity.
     *
     * @Route("/{id}", name="books_update")
     * @Method("PUT")
     * @Template("ReviewMainBundle:Review:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        if ( ! $this->checkAuth()) return $this->accessDeniedResponse();

        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('ReviewMainBundle:Review')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Review entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('books_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Review entity.
     *
     * @Route("/{id}", name="books_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        if ( ! $this->checkAuth()) return $this->accessDeniedResponse();

        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('ReviewMainBundle:Review')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Review entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('books'));
    }

    /**
     * Creates a form to delete a Review entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('books_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * Creates a form to create a Comment entity.
     *
     * @param Comment $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCommentForm($reviewId)
    {
        $comment = new Comment();
        $form = $this->createForm(new CommentType($reviewId), $comment, array(
            'action' => $this->generateUrl('comments_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Add'));

        return $form;
    }

    protected function accessDeniedResponse()
    {
        $this->get('session')->getFlashBag()->add(
            'warning',
            'Please login first'
        );

        return $this->redirect($this->generateUrl('fos_user_security_login'));
    }
    
    protected function getSecurityContext()
    {
       return $this->get('security.context');
    }

    protected function getCurrentUser()
    {
       return $this->getUser();
    }
    
    protected function checkAuth()
    {
       return $this->getSecurityContext()->isGranted('ROLE_USER');
    }
}
