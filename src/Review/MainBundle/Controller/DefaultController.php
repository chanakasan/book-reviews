<?php

namespace Review\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    function indexAction()
    {
        return $this->render('default/home.html.twig');
    }

    function aboutAction()
    {
        return $this->render('default/about.html.twig');
    }

    function contactAction()
    {
        return $this->render('default/contact.html.twig');
    }
}
